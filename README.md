# Origin Take Home Assigment (Back)

This server was developed using SpringBoot.  
This simple server is responsible for all persistence operations of the financial data and is also responsible for calculating your score.

## How to run the project ?

### Requirements

The fully fledged server uses the following:

* Apache Maven 3.6.0
* Java version: 11.0.7
* Postgres: 11
* SpringBoot

### Dependencies
There are a number of third-party dependencies used in the project. Browse the Maven pom.xml file for details of libraries and versions used.

### Building the project
You will need:

*	Java JDK 11 or higher
*	Maven 3.1.1 or higher
*	Postgres or higher
*	Git

Clone the project and use Maven to build the server

	$ mvn clean install


Create the database **origin_tha**  
Create the user **postgres** with password **postgres**  
Allow user **postgres** to full access to database **origin_tha**   



### Run the server

You can run the server locally using:

	mvn spring-boot:run


### Run the tests

You can run the test using:

    mvn test

## Structure

This project has the following structure:

- src/main/java/origin.tha.config
  - Responsible for the documentation configs 
- src/main/java/origin.tha.controller
  - Responsible for the register of all api routes
- src/main/java/origin.tha.dto
  - Responsible for process the data before deliver ir to the requester
- src/main/java/origin.tha.entity
  - Responsible for defining the data structure
- src/main/java/origin.tha.repository
  - Responsible for the communication between the model and BD
- src/main/java/origin.tha.service
  - Responsible for defining the operations and data flux os the system 
- src/main/java/origin.tha.security
  - In that case, is responsible for able the requests without any authentication
- src/main/java/origin.tha.OriginTHAApplication
  - Main class of system
- src/resources
  - Responsible for resources and assets of this system
- src/test
  - Responsible for the application tests
- request-collection.json
    - Configuration archive to import in Insomnia to test the API manually 
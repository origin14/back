package origin.tha.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import origin.tha.entity.Financial;
import origin.tha.entity.FinancialStatus;

import java.math.BigDecimal;

@Data
@JsonInclude(value = Include.NON_EMPTY)
public class FinancialDto {

    private Long id;
    private BigDecimal annualGrossIncome;
    private BigDecimal annualNetCompensation;
    private BigDecimal monthlyCost;
    private String status;
    private String statusMessage;
    private FinancialStatus statusEnum;


    public static FinancialDto fromEntity(Financial financial) {
        FinancialDto financialDto = new FinancialDto();

        financialDto.id = financial.getId();
        financialDto.annualGrossIncome = financial.getAnnualGrossIncome();
        financialDto.annualNetCompensation = financial.getAnnualNetCompensation();
        financialDto.monthlyCost = financial.getMonthlyCost();
        financialDto.status = financial.getStatus().getName();
        financialDto.statusMessage = financial.getStatus().getMessage();
        return financialDto;
    }
}
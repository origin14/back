package origin.tha.service;

import org.springframework.data.domain.Page;

public interface ServiceInterface<T, S> {

public Page<?> list(Long page, Long size, String sort, String order);
	
	public S find(Long id);
	
	public S register(T t);
	
	public S update(Long id, T t);
	
	public String delete(Long id);
	
}

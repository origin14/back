package origin.tha.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import origin.tha.dto.FinancialDto;
import origin.tha.entity.Financial;
import origin.tha.repository.FinancialRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class FinancialService implements ServiceInterface<Financial, FinancialDto> {

    final String NO_SUCH_ITEM = "ID não existe: ";

    @Autowired
    FinancialRepository financialRepository;

    public Page<FinancialDto> list(Long page, Long size, String sort, String order) {

        if (page == null) {
            page = 0L;
        }

        if (size == null) {
            size = 10L;
        }

        if (sort == null) {
            sort = "id";
        }

        PageRequest request = PageRequest.of(page.intValue(), size.intValue(), Sort.by(sort));

        if (order == null || order.equalsIgnoreCase("asc")) {
            request = PageRequest.of(page.intValue(), size.intValue(), Sort.by(sort).ascending());
        } else if (order.equalsIgnoreCase("desc")) {
            request = PageRequest.of(page.intValue(), size.intValue(), Sort.by(sort).descending());
        }

        Pageable pageable = request;

        List<FinancialDto> list = this.financialRepository.findAll(pageable).stream().map(FinancialDto::fromEntity)
            .collect(Collectors.toList());

        return new PageImpl<FinancialDto>(list, pageable, this.financialRepository.count());
    }

    public FinancialDto find(Long id) {
        return FinancialDto.fromEntity(this.financialRepository.findById(id).orElseThrow(() -> new NoSuchElementException(NO_SUCH_ITEM + id)));
    }

    public FinancialDto register(Financial t) {
        t.defineStatus();
        return FinancialDto.fromEntity(this.financialRepository.save(t));
    }

    public FinancialDto update(Long id, Financial t) {
        this.financialRepository.findById(id).orElseThrow(() -> new NoSuchElementException(NO_SUCH_ITEM + id));
        t.setId(id);
        return this.register(t);
    }

    public String delete(Long id) {
        this.financialRepository.deleteById(id);
        return "Successfully deleted";
    }
}
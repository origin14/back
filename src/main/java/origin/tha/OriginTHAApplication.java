package origin.tha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"origin.tha.*"}, exclude = {ReactiveSecurityAutoConfiguration.class})
@EntityScan(basePackages = {"origin.tha.*"})
@EnableJpaRepositories
@EnableJpaAuditing

public class OriginTHAApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(OriginTHAApplication.class, args);

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(OriginTHAApplication.class);
    }
}
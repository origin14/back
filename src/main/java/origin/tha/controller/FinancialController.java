package origin.tha.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import origin.tha.dto.FinancialDto;
import origin.tha.entity.Financial;
import origin.tha.service.FinancialService;

import javax.validation.Valid;

@CrossOrigin
@Controller
@RequestMapping("/financial")
public class FinancialController {

    @Autowired
    FinancialService financialService;

    @ResponseBody
    @GetMapping
    @ApiOperation(value = "Return a list of financial data.")
    public Page<FinancialDto> list(
        @ApiParam(value = "Desired page number.<br> <b>Default: </b> 0.") @RequestParam(name = "page", required = false) Long page,
        @ApiParam(value = "Page size. Number of items that should be listed.<br> <b>Default: </b> 10.") @RequestParam(name = "size", required = false) Long size,
        @ApiParam(value = "Attribute that will define the ordering of the list.<br> <b> Accepted values: </b> <code style='color: #9012fe'> &nbsp;id, annualIncome, monthlyCosts. </code><br> <b>Default: </b> id. ") @RequestParam(name = "sort", required = false) String sort,
        @ApiParam(value = "List order. Ascending or descending.<br> <b>Accepted values: </b> <code style='color: #9012fe'> &nbsp;asc(ascending), desc(descending). </code><br> <b>Default: </b> asc. ") @RequestParam(name = "order", required = false) String order) {
        return this.financialService.list(page, size, sort, order);
    }


    @ResponseBody
    @GetMapping("/{financialId}")
    @ApiOperation(value = "Find a financial data.")
    public FinancialDto find( @ApiParam(value = "Financial data identifier") @PathVariable("financialId") Long id) {
        return this.financialService.find(id);
    }


    @ResponseBody
    @PostMapping
    @ApiOperation(value = "Register a financial data.")
    public FinancialDto register(@ApiParam(value = "Financial data to be registered.") @Valid @RequestBody Financial financial) {
        return this.financialService.register(financial);
    }

    @ResponseBody
    @PutMapping("/{financialId}")
    @ApiOperation(value = "Update a financial data.")
    public FinancialDto update(
        @ApiParam(value = "Financial data identifier") @PathVariable("financialId") Long id,
        @ApiParam(value = "Financial data to be updated.") @RequestBody Financial financial) {
        return this.financialService.update(id, financial);
    }

    @ResponseBody
    @DeleteMapping("/{financialId}")
    @ApiOperation(value = "Delete a financial data.")
    public String delete(
        @ApiParam(value = "Financial data identifier.") @PathVariable("financialId") Long id) {
        return this.financialService.delete(id);
    }

}
package origin.tha.entity;

public enum FinancialStatus implements DetailedFinancialStatus {
    HEALTHY {
        @Override
        public String getName() {
            return "Healthy";
        }

        @Override
        public String getMessage() {
            return "Congratulations!";
        }
    },
    MEDIUM {
        @Override
        public String getName() {
            return "Average";
        }

        @Override
        public String getMessage() {
            return "There is room for improvement.";
        }
    },
    LOW {
        @Override
        public String getName() {
            return "Unhealthy";
        }

        @Override
        public String getMessage() {
            return "Caution!";
        }
    }
}

interface DetailedFinancialStatus {
    public String getName();

    public String getMessage();
}
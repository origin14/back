package origin.tha.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@Entity
@Table
@JsonInclude(value = Include.NON_EMPTY)
public class Financial extends  Core implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6687640224952830453L;

    @NotNull
    @Column(precision = 8, scale = 2)
    private BigDecimal annualGrossIncome;

    @Column(precision = 8, scale = 2)
    private BigDecimal annualNetCompensation;

    @Transient
    private final BigDecimal annualTaxes = new BigDecimal("0.08");

    @NotNull
    @Column(precision = 8, scale = 2)
    private BigDecimal monthlyCost;

    @Column
    @Enumerated(EnumType.STRING)
    private FinancialStatus status;

    public void defineStatus() {
        this.setAnnualNetCompensation(this.getAnnualGrossIncome().multiply(BigDecimal.valueOf(1).subtract(this.getAnnualTaxes())));
        BigDecimal annualCost = this.getMonthlyCost().multiply(BigDecimal.valueOf(12)).setScale(3, RoundingMode.FLOOR);
        BigDecimal score = annualCost.divide(annualNetCompensation, RoundingMode.FLOOR).setScale(3, RoundingMode.FLOOR);

        if (score.compareTo(new BigDecimal("0.25")) < 0) {
            this.setStatus(FinancialStatus.HEALTHY);
        } else if (score.compareTo(new BigDecimal("0.75")) < 1) {
            this.setStatus(FinancialStatus.MEDIUM);
        } else {
            this.setStatus(FinancialStatus.LOW);
        }

    }
}


package origin.tha.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import origin.tha.entity.Financial;
import origin.tha.entity.Financial;

@Repository
public interface FinancialRepository extends JpaRepository<Financial, Long>, JpaSpecificationExecutor<Financial> {

	Page<Financial> findAll(Pageable pageable);

	default String getName() {
		return "financialRepository";
	}
}

package origin.tha.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import origin.tha.dto.FinancialDto;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Objects;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FinancialApiPostTest {

    @LocalServerPort
    private int port;

    @Test
    public void testPostFinancialSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial";
        URI uri = new URI(baseUrl);

        HashMap<String, String> payload = new HashMap<>();
        payload.put("annualGrossIncome", "1000");
        payload.put("monthlyCost", "30");

        ResponseEntity<FinancialDto> result = restTemplate.postForEntity(uri, payload, FinancialDto.class);

        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertNotNull(Objects.requireNonNull(result.getBody()).getId());
    }

    @Test(expected = HttpClientErrorException.class)
    public void testPostFinancialErrorLetters() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial";
        URI uri = new URI(baseUrl);

        HashMap<String, String> payload = new HashMap<>();
        payload.put("annualGrossIncome", "error");
        payload.put("monthlyCost", "error");

        restTemplate.postForEntity(uri, payload, FinancialDto.class);
    }

    @Test(expected = HttpClientErrorException.class)
    public void testPostFinancialErrorWithoutArgs() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial";
        URI uri = new URI(baseUrl);

        HashMap<String, String> payload = new HashMap<>();
        payload.put("annualGrossIncome", "1000");

        restTemplate.postForEntity(uri, payload, FinancialDto.class);
    }
}

package origin.tha.financial;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import origin.tha.entity.Financial;
import origin.tha.entity.FinancialStatus;
import origin.tha.service.FinancialService;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.NoSuchElementException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FinancialApiDeleteTest {

    private final Financial financial = new Financial();

    @Autowired
    FinancialService financialService;

    @LocalServerPort
    private int port;

    @Before
    public void createFinancialData() {
        this.financial
            .setAnnualGrossIncome(new BigDecimal("1000"))
            .setMonthlyCost(new BigDecimal("10"));

        this.financialService.register(this.financial);
    }

    @Test(expected = NoSuchElementException.class)
    public void testDeleteFinancialSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial/" + this.financial.getId();
        URI uri = new URI(baseUrl);
        restTemplate.delete(uri);

        this.financialService.find(this.financial.getId());
    }
}

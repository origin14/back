package origin.tha.financial;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import origin.tha.entity.Financial;
import origin.tha.entity.FinancialStatus;
import origin.tha.service.FinancialService;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FinancialApiPutTest {

    private final Financial financial = new Financial();

    @Autowired
    FinancialService financialService;

    @LocalServerPort
    private int port;

    @Before
    public void createFinancialData() {
        this.financial
            .setAnnualGrossIncome(new BigDecimal("1000"))
            .setMonthlyCost(new BigDecimal("10"));

        this.financialService.register(this.financial);
    }

    @Test
    public void testPutFinancialSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial/" + this.financial.getId();
        URI uri = new URI(baseUrl);
        this.financial.setMonthlyCost(new BigDecimal("20"));
        restTemplate.put(uri, this.financial);

        Assert.assertEquals(0, new BigDecimal("20").compareTo(this.financialService.find(this.financial.getId()).getMonthlyCost()));
    }

    @Test
    public void testPutFinancialErrorUpdateStatus() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + port + "/financial/" + this.financial.getId();
        URI uri = new URI(baseUrl);
        this.financial.setStatus(FinancialStatus.LOW);
        restTemplate.put(uri, this.financial);

        Assert.assertNotEquals(FinancialStatus.LOW, this.financialService.find(this.financial.getId()).getStatusEnum());
    }
}

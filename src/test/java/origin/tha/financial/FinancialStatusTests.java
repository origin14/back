package origin.tha.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import origin.tha.entity.Financial;
import origin.tha.entity.FinancialStatus;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialStatusTests {

	@Test
	public void testFinancialStatusHealthy() {
		Financial financial = new Financial();
		financial
			.setAnnualGrossIncome(new BigDecimal("1000"))
			.setMonthlyCost(new BigDecimal("19"))
			.defineStatus();

		Assert.assertEquals(FinancialStatus.HEALTHY, financial.getStatus());
	}

	@Test
	public void testFinancialStatusMediumMinimum() {
		Financial financial = new Financial();
		financial
			.setAnnualGrossIncome(new BigDecimal("1000"))
			.setMonthlyCost(new BigDecimal("20"))
			.defineStatus();

		Assert.assertEquals(FinancialStatus.MEDIUM, financial.getStatus());
	}

	@Test
	public void testFinancialStatusMediumMaximun() {
		Financial financialLimitGreater = new Financial();
		financialLimitGreater
			.setAnnualGrossIncome(new BigDecimal("1000"))
			.setMonthlyCost(new BigDecimal("57.5"))
			.defineStatus();

		Assert.assertEquals(FinancialStatus.MEDIUM, financialLimitGreater.getStatus());
	}

	@Test
	public void testFinancialStatusLow() {
		Financial financial = new Financial();
		financial
			.setAnnualGrossIncome(new BigDecimal("1000"))
			.setMonthlyCost(new BigDecimal("57.6"))
			.defineStatus();

		Assert.assertEquals(FinancialStatus.LOW, financial.getStatus());
	}
}
